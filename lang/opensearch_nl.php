<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/opensearch?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_boite_opensearch' => 'Configuratie van plugin OpenSearch',
	'cfg_descr_opensearch' => 'OpenSearch: Gepersonaliseerde zoekmachine voor je site.',
	'cfg_inf_id_rubrique' => 'Kies de rubriek waarin gezocht moet worden.',
	'cfg_inf_page_recherche' => 'Geef het te gebruiken skelet voor het zoeken op de site (bijvoorbeeld: "recherche" voor het skelet recherche.html)',
	'cfg_lbl_id_rubrique' => 'Zoekrubriek',
	'cfg_lbl_page_recherche' => 'Zoekskelet',
	'cfg_option_tout' => 'De gehele site',
	'cfg_titre_opensearch' => 'OpenSearch',
];
