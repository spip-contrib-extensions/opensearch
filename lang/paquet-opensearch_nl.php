<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-opensearch?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// O
	'opensearch_description' => 'Plugin die bezoekers kan voorstellen om jouw site als aangepaste zoekmachine aan hun browser toe te voegen.',
	'opensearch_slogan' => 'Jouw site als zoekmachine',
];
