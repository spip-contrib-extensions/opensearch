<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-opensearch?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// O
	'opensearch_description' => 'Plugin which enables to offer the users to add a custom search engine related to your website on their browser.',
	'opensearch_slogan' => 'Your website as search engine',
];
