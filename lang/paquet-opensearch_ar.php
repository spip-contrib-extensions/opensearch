<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-opensearch?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// O
	'opensearch_description' => 'هذا الملحق يتيح لزوار موقعك إضافة محرك بحث مخصص لمتصفحهم.',
	'opensearch_slogan' => 'حول موقعك الى محرك بحث',
];
