<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/opensearch?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_boite_opensearch' => 'Configurazione del plugin OpenSearch',
	'cfg_descr_opensearch' => 'OpenSearch: motore di ricerca personalizzato per il tuo sito',
	'cfg_inf_id_rubrique' => 'Seleziona la rubrica in cui effettuare la ricerca',
	'cfg_inf_page_recherche' => 'Indica il template da utilizzare per la ricerca sul sito (esempio: cerca il template search.html)',
	'cfg_lbl_id_rubrique' => 'Obiettivo',
	'cfg_lbl_page_recherche' => 'Template di ricerca',
	'cfg_option_tout' => 'Tutto il sito',
	'cfg_titre_opensearch' => 'OpenSearch',
];
