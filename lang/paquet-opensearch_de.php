<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-opensearch?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// O
	'opensearch_description' => 'Durch dieses Plugin können besucher Ihre Website als Suchmaschine in ihren Webbrowser eintragen.',
	'opensearch_slogan' => 'Ihre Website als Suchmaschine nutzen',
];
