<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/opensearch?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_boite_opensearch' => 'إعداد ملحق البحث المفتوح OpenSearch',
	'cfg_descr_opensearch' => 'البحث المفتوح OpenSearch: محرك بحث مخصص لموقعك.',
	'cfg_inf_id_rubrique' => 'تحديد القسم الذي سيتم البحث فيه.',
	'cfg_inf_page_recherche' => 'تحديد الصفحة النموذجية التي ستستخدم للبحث في الموقع (مثلاً: البحث للصفحة النموذجية recherche.html)',
	'cfg_lbl_id_rubrique' => 'القسم الهدف',
	'cfg_lbl_page_recherche' => 'الصفحة النموذجية للبحث',
	'cfg_option_tout' => 'كل الموقع',
	'cfg_titre_opensearch' => 'البحث المفتوح OpenSearch',
];
