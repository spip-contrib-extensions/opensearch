<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/opensearch?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'cfg_boite_opensearch' => 'OpenSearch plugin configuration',
	'cfg_descr_opensearch' => 'OpenSearch: custom search engine for your website.',
	'cfg_inf_id_rubrique' => 'Select the section in which to search.',
	'cfg_inf_page_recherche' => 'Specify the skeleton to use for the site search (exemple : look for the recherche.html skeleton)',
	'cfg_lbl_id_rubrique' => 'Target section',
	'cfg_lbl_page_recherche' => 'Search skeleton',
	'cfg_option_tout' => 'The whole website',
	'cfg_titre_opensearch' => 'OpenSearch',
];
