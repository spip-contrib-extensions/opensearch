<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-opensearch?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// O
	'opensearch_description' => 'Plugin che consente ai visitatori del tuo sito di aggiungere un motore di ricerca personalizzato al proprio browser.',
	'opensearch_slogan' => 'Il tuo sito come motore di ricerca',
];
