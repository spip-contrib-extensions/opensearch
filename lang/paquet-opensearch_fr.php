<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/opensearch.git

return [

	// O
	'opensearch_description' => 'Plugin qui permet de proposer aux visiteurs de votre site d’ajouter un moteur de recherche personnalisée à leur navigateur.',
	'opensearch_slogan' => 'Votre site comme moteur de recherche',
];
