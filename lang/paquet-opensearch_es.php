<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-opensearch?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// O
	'opensearch_description' => 'Plugin que permite proponer a los visitantes de su sitio añadir un motor de búsqueda personalizado para su navegador. ',
	'opensearch_slogan' => 'Su sitio como motor de búsqueda',
];
